#ifndef _MUTATOR_H_
#define _MUTATOR_H_

#include "Individual.h"

class Mutator
{
    public:
        //Pure Virtual function that is defined by child class
        virtual Individual mutate(Individual ind, int k) = 0;
};

#endif