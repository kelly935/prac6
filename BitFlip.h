#ifndef _BITFLIP_H
#define _BITFLIP_H

#include "Mutator.h"

class BitFlip : public Mutator
{
    public:
        //Flips the bit in the Individual bitstring at location k (Circular Wrapped)
        Individual mutate(Individual ind, int k);
};

#endif