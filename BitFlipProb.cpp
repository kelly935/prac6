#include <cstdlib>
#include <ctime>
#include <iostream>
#include "BitFlipProb.h"

BitFlipProb::BitFlipProb(double pSet)
{
    //This sets the seed by using the current system time (ensures random seed each time)
    std::srand(std::time(nullptr));
    p = pSet; //probability of bit flip
}

Individual BitFlipProb::mutate(Individual ind, int k)
{
    int len = ind.getLength();
    for(int i = 0; i < len; i++)
    {
        int random_choice = std::rand() % 100; //Gives a random percentage chance from 0 - 1
    
        //Checks if we should flip bit based on the chance p and the random choice
        if(random_choice > 100 - (p*100))
        {
            ind.flipBit(i);
        }
    }

    return ind;
}