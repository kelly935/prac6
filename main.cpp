#include <iostream>
#include <sstream>
#include "Individual.h"
#include "BitFlip.h"
#include "BitFlipProb.h"
#include "Rearrange.h"

Individual * execute(Individual * indPtr, Mutator * mPtr, int k)
{
    //Need to create a copy to protect the pointer to the Individual passed in
    Individual* result = new Individual(indPtr->getString());
    
    *result = mPtr->mutate(*indPtr, k);
    return result;
}

int main()
{
    //Get line of user input
    std::string input;
    getline(std::cin, input);

    //Use a string stream to capture each of the values
    std::string binStr1, binStr2, k1Str, k2Str;
    std::stringstream stringstream(input);
    stringstream >> binStr1 >> k1Str >> binStr2 >> k2Str;

    //Convert the k values to integets
    int k1 = stoi(k1Str);
    int k2 = stoi(k2Str);

    //Create two individuals using binary strings
    Individual* i1 = new Individual(binStr1);
    Individual* i2 = new Individual(binStr2);
    
    //Create modifiers
    BitFlip bf;
    Rearrange re;

    //Run Modifiers
    //i1 = bf.mutate(i1, k1);
    i1 = execute(i1, &bf, k1);

    //i2 = re.mutate(i2, k2);
    i2 = execute(i2, &re, k2);

    std::cout << i1->getString() << " " << i2->getString() << " " << i2->getMaxOnes() << std::endl;
}