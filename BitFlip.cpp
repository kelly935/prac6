#include "BitFlip.h"

Individual BitFlip::mutate(Individual ind, int k)
{
    int len = ind.getLength();
    k--; //because we are starting at 1 not 0.
    k %= len; //Wrap k so that it cannot go out of bounds;

    ind.flipBit(k);
    return ind;
}