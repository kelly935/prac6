#ifndef _REARRANGE_H_
#define _REARRANGE_H_

#include "Mutator.h"

class Rearrange : public Mutator
{
    public:
        //Sets the bit k as the new start of the bitstring (Circular Wrapped)
        Individual mutate(Individual ind, int k);
};

#endif