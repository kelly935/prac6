#include "Individual.h"

Individual::Individual(int startLength)
{
    //We create a binary string of zeros based on the length provided
    binaryString = "";
    for(int i = 0; i < startLength; i++)
    {
        binaryString.append("0");
    }
}

Individual::Individual(std::string startString)
{
    //Just copy the string across
    binaryString = startString;
}

std::string Individual::getString()
{
    return binaryString;
}

int Individual::getBit(int pos)
{
    //Make sure that we are within the boundaries of the string
    if((unsigned int) pos >= binaryString.length())
    {
        //Otherwise return -1
        return -1;
    }
    
    if (binaryString[pos] == '1')
    {
        return 1;
    }

    if(binaryString[pos] == '0')
    {
        return 0;
    }

    //If we get here there is something wrong with the binary string, just return -1
    return -1;
}

void Individual::flipBit(int pos)
{
    //Make sure that we are within the boundaries of the string
    if((unsigned int) pos >= binaryString.length())
    {
        //Do nothing - Should there be an error here?
    }
    
    else if (binaryString[pos] == '1')
    {
        binaryString[pos] = '0';
    }

    else if(binaryString[pos] == '0')
    {
        binaryString[pos] = '1';
    }
}

int Individual::getMaxOnes()
{
    int num_ones = 0;
    int max_ones = 0;
    for(unsigned int i = 0; i < binaryString.length(); i++)
    {
        //If we see a one we add to the counter
        if (binaryString[i] == '1')
        {
            num_ones++;

            //If we go above our prev max value we set the max value again
            if(num_ones > max_ones)
            {
                max_ones = num_ones;
            }
        }

        //If we see a zero we reset our counter
        else if(binaryString[i] == '0')
        {
            num_ones = 0;
        }
    }

    return max_ones;
}

int Individual::getLength()
{
    return binaryString.length();
}