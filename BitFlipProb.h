#ifndef _BITFLIPPROB_H
#define _BITFLIPPROB_H

#include "Mutator.h"

class BitFlipProb : public Mutator
{
    public:
        //Sets the probability of flipping a bit in bitstring of Individual
        BitFlipProb(double pSet);

        //Randomly flips bits in bitstring for an Individual based on probability (K is not actually used at all)
        Individual mutate(Individual ind, int k);

    private:
        //Probability of a bitflip occuring (for each bit)
        double p = 0.5;
};

#endif