#ifndef _INDIVIDUAL_H_
#define _INDIVIDUAL_H_

#include <string>

class Individual
{
    public:
        Individual(int startLength);
        Individual(std::string startString);

        std::string getString();
        int getBit(int pos);
        void flipBit(int pos);
        //Get the max number of consecutive ones in bitstring
        int getMaxOnes();
        int getLength();

    private:
        std::string binaryString;
};

#endif