#include "Rearrange.h"

Individual Rearrange::mutate(Individual ind, int k)
{
    int len = ind.getLength();
    k--; //because we are starting at 1 not 0.
    k %= len; //Wrap k so that it cannot go out of bounds;

    std::string tmp = "";

    int offset = 0;
    for(int i = 0; i < len; i++)
    {
        offset = i + k;
        offset %= len; //Wrap offset so that it cannot go out of bounds;

        if(ind.getBit(offset) == 0)
        {
            tmp.append("0");
        }

        else if(ind.getBit(offset) == 1)
        {
            tmp.append("1");
        }
        
    }

    //Create a new Individual with bitstring tmp and return it
    Individual result = Individual(tmp);
    return result;
}